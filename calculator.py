from flask import Flask , render_template , request
####### Main Modules 
import calendar
import datetime
from datetime import date
from flask.templating import render_template_string 


app = Flask(__name__)

@app.route('/')

def Home():
    return render_template('navbar.html',title="HOME")

@app.route('/basic')

def Calc():
    return render_template('index.html',title= 'Basic Calculator')

@app.route('/Menu')

def Menu():
    return render_template('MENU.html')

@app.route('/Age')
def Age():
    months = []
    current_year = date.today().year
    months = [ (1,'January'), (2,'February'), (3,'March'), (4,'April'), (5,'May'), (6,'June'), (7,'July'), (8,'August'), (9,'September'), (10,'October'), (11,'November'), (12,'December')]
    day = [x for x in range(1,32)]
    year = [x for x in range(1900,current_year+1)]
    return render_template('Age.html',months=months,day=day,year=year,title='Age Calculator')

@app.route("/" , methods=['GET','POST'])
def main():
    if request.method == 'POST':
        day_u = int(request.form.get('Day'))
        mon =int(request.form.get('mon'))
        y =int( request.form.get('My')) 

        def leep(y):
            if y%4==0 and y%100!=0 or y%400==0:
                return True
            return False 

        if leep(y) and mon == 2 and day_u > 29:
            return '<h1> Hay It is a leep year Day must be 29 or less </h1>'
        elif not leep(y) and mon == 2 and day_u > 28:
            return "<h1> Hay It Feb Has Only 28 Days </h1>"
        elif leep(y) and mon == 2 and day_u == 29:
            return 'Hay you born at leep year '
        
        if  day_u > 30 and mon!=1 and mon!=2 and mon!=3 and mon!=5 and mon!=7 and mon!=8 and mon!=10 and mon!=12:
            return "<h1> Hay April and June and September and November Has 30 days You given more than that </h>"
        
        
        
        now_day = str(date.today())
        lis = now_day.split('-')
        enter_date = int(lis[2])
        enter_month= int(lis[1])
        enter_year = int(lis[0])
        mone = [31,28,31,30,31,30,31,31,30,31,30,31]
        if day_u>enter_date:
            enter_month = enter_month-1 
            enter_date = enter_date+mone[mon-1]
        if mon > enter_month:
            enter_year = enter_year-1
            enter_month = enter_month+12 
        date_resul = str(enter_date- day_u)
        mont_re = str(enter_month- mon)
        yer_result = str(enter_year- y)

        Now_day = datetime.datetime.now().day
        Now_m = datetime.datetime.now().month
        Now_y = datetime.datetime.now().year
        birth = datetime.date(y,mon,day_u)
        today = datetime.date.today()

        if (today.month == birth.month and today.day >= birth.day  or today.month > birth.month  ):
            nexty = today.year +1 
        else:
            nexty = today.year
        
        ne = datetime.date(nexty,birth.month , birth.day)
        deff = ne - today
        ab  = str(deff)
        ef = ab.split('0:00:00')
        ret = ef[0]
        

        return render_template('data.html',date_resul=date_resul,mont_re=mont_re,yer_result=yer_result,deff=deff,ret=ret,ne=ne,title='Age Data')#birth_day=birth_day,birth_mon=birth_mon,birth_yer=birth_yer )

@app.route('/BMI')

def BMI():
    BMI_units = ['Centimeter','Meter',"Feet",'Inches','Kilograms','Pounds']
    return render_template('BMI.html',BMI_units=BMI_units,title='BMI')

@app.route('/B',methods=['GET','POST'] )
def Bmi_cal():

    if request.method == 'POST':

        hight = (request.form['Hight'])
        wight = (request.form['wight'])

        ## cheking for value error 
        try:
            hight = float(hight)
            wight = float(wight)
        except:
            return '<h1> Value Error </h1>'

        mass_bmi = (request.form.get('mass'))
        hight_bmi =  (request.form.get('hig'))

        bmi_convert = {
        'Centimeter': (hight / 100) ,"Feet": (hight/3.2808) ,"Inches":(hight/39.37),'Pounds': (wight*2.205) , 'Kilograms':(wight*1) ,'Meter':round(hight*1)}

        mass_val = (bmi_convert[mass_bmi])
        hight_val = (bmi_convert[hight_bmi])
        bmi = round(mass_val/(hight_val*hight_val),1)

        return render_template('bmi_data.html',bmi=bmi,title='Data')

@app.route('/currency')

def currency():
    global c 
    c = CurrencyRates()
    data = sorted((c.get_rates('USD').keys()))
    print(len(data))
    return render_template('currency.html',data=data)
#     retur = c.convert(option2,option1,1)
@app.route('/CU',methods=['GET','POST'])
def url():
    if request.method == 'POST':
        curr_r = CurrencyRates()
        first = (request.form['first'])
        try:
            first = float(first)
        except:
            return '<h1> Value Error </h1>'

        option1 = request.form.get('Con') 
        option2 = request.form.get('con2')
        retur = curr_r.convert(option1,option2,first)
        return render_template('currency.html',retur=retur,title='currency')

@app.route('/Temp')

def Temp():
    Temp_data = [('C','Celsius'),('F','Fahrenheit'),('K','Kelvin'),("R",'Rankine') ,('Re','Réaumur')]
    return render_template('temp.html',Temp_data=Temp_data,title='Temperate') 

@app.route('/Tcalculation',methods=['GET','POST'])
def Temp_c():
    if  request.method == 'POST':
        Temp_data = [('C','Celsius'),('F','Fahrenheit'),('K','Kelvin'),("R",'Rankine') ,('Re','Réaumur')]
        user_temp = request.form.get('user') 
        Convert_to = request.form.get('cal')
        input_data = request.form.get('VAL')
        try:
            input_data = float(input_data)
        except:
            return '<h1> Value Error </h1>'
        Unit_value = user_temp+Convert_to
        TEMP_CONV = { 
            'CC':input_data*1 ,
            'CK':input_data + 273.15 ,
            'CF':(input_data * 1.8) + 32,
            'CR':(input_data+273.15)*1.8,
            'CRe':input_data* 0.80000,

            'KC':input_data-273,
            'KK':input_data*1,
            'KF':(input_data - 273.15)* 1.8000+ 32.00,
            'KR':input_data*1.8,
            'KRe':(input_data - 273.15)* 0.80000,

            'FC':(input_data - 32) / 1.8,
            'FK':(input_data - 32) / 1.8 + 273.15,
            'FF':input_data*1,
            'FR':input_data + 459.67,
            'FRe':(input_data - 32)* 0.44444,

            'RC': (input_data - 491.67) / 1.8,
            'RK':input_data / 1.8,
            'RR':input_data*1,
            'RF': input_data - 459.67,
            'RRe':(input_data - 491.67)* 0.44444,

            'ReC':input_data/0.80000,
            'ReK': (input_data/0.80000)+273.15,
            'ReRe':input_data*1,
            'ReF':(input_data* 2.2500)+ 32.00,
            'ReR':(input_data* 2.2500)+ 491.67
            }

        UNIT_out = round(TEMP_CONV[Unit_value],3)

        return render_template('temp.html',UNIT_out=UNIT_out,Temp_data=Temp_data,title='Temperature')
   
@app.route('/Time Converter')

def Time_conv():

    Time_conv_data = [('Y','Year'),('WK','Week'),('D','Day'),('H','Hour'),('MI','Minute'),('S','Second'),('MS','Millisecond'),('US','Microsecond'),('PS','Picosecond')]

    return render_template('Time_Converter.html',Time_conv_data=Time_conv_data,title='Time Converter')

@app.route('/Time_con',methods=['GET','POST'])

def Time_con():
    if  request.method == 'POST':
        Time_user_data1 = request.form.get("From_data")
        Time_user_data2 = request.form.get("To_data")
        Uset_Time_data = request.form.get("User_num")
        Time_conv_data = [('Y','Year'),('WK','Week'),('D','Day'),('H','Hour'),('MI','Minute'),('S','Second'),('MS','Millisecond'),('US','Microsecond'),('PS','Picosecond')]
        try:
            Uset_Time_data = float(Uset_Time_data)
        except:
            return '<h1> Value Error </h1>'
        
        if Time_user_data1 == Time_user_data2:
            TINE_OUT = Uset_Time_data*1

        else:
            add_time=Time_user_data1 + Time_user_data2
            T_conv  = {
            ## For Year
            'YWK':Uset_Time_data*52.17857,
            'YD': Uset_Time_data*365,
            'YH' :Uset_Time_data*8766,
            'YMI':Uset_Time_data*525960,
            'YS' :Uset_Time_data*31557600,
            'YMS':Uset_Time_data*31557600000,
            'YUS':Uset_Time_data*31557600000000,
            'YPS':Uset_Time_data*31557600000000000000,

            'WKY': Uset_Time_data / 52.14285714,
            'WKD':Uset_Time_data*7,
            'WKH':Uset_Time_data*168,
            'WKMI':Uset_Time_data*10080,
            'WKS':Uset_Time_data*604800,
            'WKMS':Uset_Time_data*604800000,
            'WKUS':Uset_Time_data*604800000000,
            'WKPS':Uset_Time_data*604800000000000000,

            'DY':Uset_Time_data / 365,
            'DWK': Uset_Time_data / 7,
            'DH': Uset_Time_data * 24,
            'DMI':Uset_Time_data * 1440,
            'DS':Uset_Time_data * 86400,
            'DMS': Uset_Time_data *86400000,
            'DUS': Uset_Time_data * 86400000000,
            'DPS':Uset_Time_data *86400000000000000,

            'HY':Uset_Time_data / 8760,
            'HWK': Uset_Time_data / 168,
            'HD':Uset_Time_data / 24,
            'HMI':Uset_Time_data * 60,
            'HS': Uset_Time_data * 3600,
            'HMS': Uset_Time_data *3600000 ,
            'HUS': Uset_Time_data *3600000000,
            'HPS':Uset_Time_data *3600000000000000,

            'MIY':Uset_Time_data* 525600,
            'MIWK': Uset_Time_data / 10080,
            'MID' : Uset_Time_data /1440,
            'MIS': Uset_Time_data *60,
            'MIH': Uset_Time_data / 60,
            'MIMS':Uset_Time_data * 60000,
            'MIUS': Uset_Time_data*60000000,
            'MIPS': Uset_Time_data *60000000000000,

            'SY': Uset_Time_data / 31556952,
            'SWK': Uset_Time_data/604800,
            'SD':Uset_Time_data/  86400,
            'SH': Uset_Time_data / 3600,
            'SMS': Uset_Time_data * 1000,
            'SMI': Uset_Time_data /60,
            'SUS': Uset_Time_data * 1000000,
            'SPS':  Uset_Time_data * 1000000000000,

            'MSY':Uset_Time_data / 31556952000,
            'MSWK':Uset_Time_data / 604800000,
            'MSH' :Uset_Time_data / 3600000,
            'MSMI':Uset_Time_data / 60000,
            'MSD': (Uset_Time_data / (1000*60*60*24))%7 ,
            'MSS':Uset_Time_data / 100,
            'MSUS': Uset_Time_data *100,
            'MSPS' : Uset_Time_data * 1000000000,
            
            'USY': Uset_Time_data / 31556952000000,
            'USWK':Uset_Time_data / 604800000000,
            'USD': Uset_Time_data / 86400000000,
            'USMI':Uset_Time_data /  60000000,
            'USH':Uset_Time_data / 3600000000,
            'USS':Uset_Time_data /  1000000,
            'USMS':Uset_Time_data/1000,
            'USPS':Uset_Time_data* 1000000,

            'PSY': (Uset_Time_data/9223000000000001024),
            'PSWK':Uset_Time_data/604800000000000000,
            'PSD': Uset_Time_data/86400000000000000,
            'PSMI':Uset_Time_data/60000000000000,
            'PSH':Uset_Time_data / 3600000000000000,
            'PSS':Uset_Time_data/100000000000,
            'PSMS':Uset_Time_data/1000000000,
            'PSUS':Uset_Time_data/1000000
             }
            TINE_OUT = T_conv[add_time]

        return render_template('Time_Converter.html',TINE_OUT=TINE_OUT,Time_conv_data=Time_conv_data)



@app.route('/Data converter')

def Data_con():
    Data_inlis = [('B','Byte'),('KB','Kilobyte'),('MB','Megabyte'),('GB','Gigabyte'),('TB','Tebibyte'),('PB','Petabyte')]
    return render_template('MbGB.html',Data_inlis=Data_inlis)

@app.route('/Data',methods=['GET','POST'])
def user_data_value():
    if request.method == 'POST':
        num_b = request.form.get("inbox")
        Binary_data_from_conv = request.form.get('One')
        Binary_data_to_conv = request.form.get('c')
        Data_inlis = [('B','Byte'),('KB','Kilobyte'),('MB','Megabyte'),('GB','Gigabyte'),('TB','Tebibyte'),('PB','Petabyte')]

        try:
            num_b = float(num_b) 
        except:
            return 'Error value'

        Binary_value = str(Binary_data_from_conv)+str(Binary_data_to_conv)
        print(Binary_data_from_conv,Binary_data_to_conv)
        Binary_data_calculation= {
            'BB':num_b*1,'KBKB':num_b*1,'MBMB':num_b*1,'GBGB':num_b*1,'TBTB':num_b*1 ,'PBPB':num_b*1,
            # for byts
            'BKB':num_b/1024,'BMB':num_b/1048576,'BGB':num_b/1073741824,
            'BTB':num_b/1099511627776,'BPB':num_b/ 1125899906842620,
            # For KB 
            'KBB':num_b*1024,'KBMB':num_b/1024 ,'KBGB':num_b/1048576,
            'KBTB':num_b/1073741824,'KBPB':num_b/1099511627776,
            #MB
            'MBB':num_b*1048576,'MBKB':num_b*1024,'MBGB':num_b/1024,
            'MBTB':num_b/1048576,'MBPB':num_b/1073741824,
            #GB
            'GBB':num_b*1073741824,'GBKB':num_b*1048576,'GBMB':num_b*1024,
            'GBTB':num_b/1024,'GBPB':num_b/1048576,
            #TB
            'TBB':num_b*1099511627776 ,'TBMB':num_b*1048576,'TBGB':num_b* 1024,
            'TBPB':num_b/1024,'TBKB':num_b*1073741824,
            #PB
            'PBB':num_b*1125899906842620,'PBMB':num_b*1073741824,'PBKB':num_b*1099511627776,
            'PBGB':num_b*1048576,'PBTB':num_b*1024}

        out_bin_data = Binary_data_calculation[Binary_value]

        return render_template('MbGB.html',out_bin_data=out_bin_data,Data_inlis=Data_inlis)
  
@app.route('/Speed')
def Speed():
    speed_data = [('MPH','Miles-per-hour'),('FPS','Feet-per-second'),('MPS','Meter-per-second'),
    ('KPH','Kilometer-per-hour'),('Kno','Knot')]
    return render_template('SPEED.html',speed_data=speed_data)
@app.route('/speed',methods=['GET',"POST"])
def speed():
    if request.method == 'POST':
        speed_data = [('MPH','Miles-per-hour'),('FPS','Feet-per-second'),('MPS','Meter-per-second'),
    ('KPH','Kilometer-per-hour'),('Kno','Knot')]
        user_speed =  request.form.get('speed_val')
        user_set_data = request.form.get('data')
        user_set_2 = request.form.get('data2')
        add_of_speed = user_set_data +user_set_2
        try:
            user_speed = float(user_speed)
        except:
            return '<script>alert("Hay Please input Number");</script>'
        dic_of_data = {
            'MPHFPS':user_speed*1.466667,'MPHMPS':user_speed*0.44704,'MPHMPH':user_speed*1,
            'MPHKPH':user_speed* 1.609344,'MPHKno':user_speed/1.15078,
            'KPHMPH':user_speed/1.609344,'KPHKPH':user_speed*1,'KPHFPS':user_speed*0.911344,
            'KPHMPS':user_speed/3.6,'KPHKno':user_speed*0.539957,
            'FPSMPH':user_speed*0.681818 ,'FPSMPS':user_speed* 0.3048,'FPSKPH':user_speed* 1.09728,'FPSFPS':user_speed*1,
            'FPSKno':user_speed*0.592484,
            'MPSMPH':user_speed/0.44704 ,'MPSKPH':user_speed*3.6,'MPSMPS':user_speed*1,'MPSFPS':user_speed/0.3048,
            'MPSKno':user_speed*1.943844,
            'KnoMPS':user_speed*0.514444,'KnoKno':user_speed*1,'KnoMPH':user_speed*1.150779,
            'KnoKPH':user_speed* 1.852,'KnoFPS':user_speed*1.69}
        speed_out = dic_of_data[add_of_speed]
        return render_template('SPEED.html',speed_data=speed_data,speed_out=speed_out)

@app.route('/mass')
def mass():
    mass_data = [('T','Tonne'),('K','Kilogram'),('G','Gram'),('MI','Milligram'),('MIC','Microgram'),('IT','Imperial-Ton'),('US','US-Ton'),('ST','Stone'),('P','Pound'),('OU','Ounce')]
    return render_template('Mass.html',mass_data=mass_data)
@app.route('/M_data',methods=['GET','POST'])
def M_data():
    if request.method == 'POST':
        mass_data = [('T','Tonne'),('K','Kilogram'),('G','Gram'),('MI','Milligram'),('MIC','Microgram'),('IT','Imperial-Ton'),('US','US-Ton'),('ST','Stone'),('P','Pound'),('OU','Ounce')]
        user_mas = request.form.get('user_data')
        mas_1 = request.form.get('from')
        mas_2 = request.form.get('To')
        try:
             user_mas = float(user_mas)
        except:
            return 'Value Error'
        if mas_1 == mas_2:
            dummy_dat = user_mas*1
        else:
            add_mass = mas_1+mas_2
            mass_cal = {
                'TK':user_mas*1000,'TG':user_mas*1000000,'TMI':user_mas*1000000,'TMIC':user_mas*1000000000000,'TIT':user_mas*0.984206 ,'TUS':user_mas*1.102311        
            } 
            dummy_dat =   mass_cal[add_mass]
        return render_template('Mass.html',mass_data=mass_data,dummy_dat=dummy_dat)

    



    

if __name__ == '__main__':
    app.run(debug=True)
